/***************************************************************************

 This program may be modified and copied freely according to the terms of
 the GNU general public license (GPL), as long as the above copyright
 notice and the licensing information contained herein are preserved.

 Please refer to www.gnu.org for licensing details.

 This work is provided AS IS, without warranty of any kind, expressed or
 implied, including but not limited to the warranties of merchantability,
 noninfringement, and fitness for a specific purpose. The author will not
 be held liable for any damage caused by this work or derivatives of it.

 By using this source code, you agree to the licensing terms as stated
 above.

 @ Last-Modified: 19-01-2014

 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "chash.h"

#include <string.h>

unsigned long stdhash(const void *value) {
  unsigned long hash = 49613;
  int c;
  while ((c = *(char *) value++))
  {
    hash = ((hash << 5) + hash) + c;
  }
  return hash;
}

int stdcompare(const void *x, const void *y) {
  return strcmp((const char *) x, (const char *) y);
}

int hashtable_searchslot(hashtable_t *tbl, void *key) {
  int index, c = 0;
  index = tbl->hash(key) & (tbl->capacity - 1);

  while ((tbl->buckets[index].key != NULL) && (c < OPEN_ADDRESS_LINEAR_LENGTH))
  {
    if (!tbl->compare(tbl->buckets[index].key, key))
    {
      return index;
    }
    c++;
    index = (index + 1) & (tbl->capacity - 1);
  }
  return -1;
}

void hashtable_rebuild(hashtable_t *tbl) {
  bucket_t * temp = tbl->buckets;
  tbl->size = 0;
  tbl->buckets = (bucket_t *) calloc(tbl->capacity, sizeof(bucket_t));

  for (int i = 0; i < tbl->capacity; i++)
  {
    if (temp[i].key != NULL)
    {
      hashtable_access(tbl, temp[i].key, temp[i].value);
    }
  }
  free(temp);
}

void hashtable_remove(hashtable_t *tbl, void * key) {
  int id, next;
  if ((id = hashtable_searchslot(tbl, key)) >= 0)
  {

    free(tbl->buckets[id].key);
    tbl->buckets[id].key = NULL;

    free(tbl->buckets[id].value);
    tbl->buckets[id].value = NULL;

    if (tbl->capacity > 1)
    {
      for (int i = 1; i < OPEN_ADDRESS_LINEAR_LENGTH; i++)
      {
        next = (id + i) & (tbl->capacity - 1);
        if (tbl->buckets[next].key != NULL)
        {
          if ((tbl->hash(tbl->buckets[next].key) & (tbl->capacity - 1)) == id)
          {
            hashtable_rebuild(tbl);
          }
        }
      }
    }
    tbl->size--;
  }
}

hashtable_t *
hashtable_alloc(hash_fn_t hash_fn, compare_fn_t compare_fn, unsigned char size_exponent) {
  hashtable_t * htable = (hashtable_t *) malloc(sizeof(hashtable_t));

  htable->capacity = (size_exponent > 0) ? 2 << (size_exponent - 1) : 1;
  htable->size = 0;
  htable->hash = hash_fn;
  htable->compare = compare_fn;

  htable->buckets = (bucket_t *) calloc(htable->capacity, sizeof(bucket_t));
  return htable;
}

void hashtable_free(hashtable_t *tbl, void (*free_key)(void *), void (*free_value)(void *)) {
  if (free_key != NULL || free_value != NULL)
  {
    for (int i = 0; i < tbl->capacity; i++)
    {
      if (tbl->buckets[i].key != NULL)
      {
        if (free_key != NULL)
        {
          free_key(tbl->buckets[i].key);
        }
        if (free_value != NULL)
        {
          free_value(tbl->buckets[i].value);
        }
      }
    }
  }
  free(tbl->buckets);
  free(tbl);
}

void ** hashtable_access(hashtable_t *tbl, void *key, void *value) {
  unsigned int index;
  index = tbl->hash(key) & (tbl->capacity - 1);

  int c = 0;

  while ((tbl->buckets[index].key != NULL) && (c < OPEN_ADDRESS_LINEAR_LENGTH))
  {

    if (!tbl->compare(tbl->buckets[index].key, key))
    {
      return &tbl->buckets[index].value;
    }
    c++;
    index = (index + 1) & (tbl->capacity - 1);
  }

  if (value != NULL)
  {
    if (c < OPEN_ADDRESS_LINEAR_LENGTH)
    {
      tbl->buckets[index].key = key;
      tbl->buckets[index].value = value;
    } else
    {
      bucket_t * tmp = tbl->buckets;

      tbl->capacity *= 2;
      tbl->size = 0;
      tbl->buckets = (bucket_t *) calloc(tbl->capacity, sizeof(bucket_t));

      for (int i = 0; i < (tbl->capacity / 2); i++)
      {
        if (tmp[i].key != NULL)
        {
          hashtable_access(tbl, tmp[i].key, tmp[i].value);
        }
      }
      free(tmp);
      hashtable_access(tbl, key, value);
      tbl->size--;
    }
    tbl->size++;
  }
  return NULL;
}

void hashtable_foreach(hashtable_t *tbl, visit_fn_t visit, void *state) {
  for (int i = 0; i < tbl->capacity; i++)
  {
    if (tbl->buckets[i].key != NULL)
    {
      visit(tbl->buckets[i].key, tbl->buckets[i].value, state);
    }
  }
}

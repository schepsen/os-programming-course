/***************************************************************************
 Copyright (C) 2013 Christoph Reichenbach


 This program may be modified and copied freely according to the terms of
 the GNU general public license (GPL), as long as the above copyright
 notice and the licensing information contained herein are preserved.

 Please refer to www.gnu.org for licensing details.

 This work is provided AS IS, without warranty of any kind, expressed or
 implied, including but not limited to the warranties of merchantability,
 noninfringement, and fitness for a specific purpose. The author will not
 be held liable for any damage caused by this work or derivatives of it.

 By using this source code, you agree to the licensing terms as stated
 above.


 Please contact the maintainer for bug reports or inquiries.

 Current Maintainer:

 Christoph Reichenbach (CR) <creichen@gmail.com>

 ***************************************************************************/

#ifndef _CHASH_H
#define _CHASH_H

#define OPEN_ADDRESS_LINEAR_LENGTH 4

typedef int (*compare_fn_t)(const void *, const void *);
typedef unsigned long (*hash_fn_t)(const void *);
typedef void (*visit_fn_t)(void *key, void *value, void *state);

unsigned long stdhash(const void *value);
int stdcompare(const void *x, const void *y);

typedef struct bucket
{
  void *key;
  void *value;
} bucket_t;

typedef struct hashtable
{
  unsigned capacity, size;
  hash_fn_t hash;
  compare_fn_t compare;
  bucket_t *buckets;
} hashtable_t;

hash_fn_t string_hash;

hashtable_t *hashtable_alloc(hash_fn_t hash_fn, compare_fn_t compare_fn, unsigned char size_exponent);
void hashtable_free(hashtable_t *tbl, void (*free_key)(void *), void (*free_value)(void *));
void hashtable_remove(hashtable_t *tbl, void *key);
void **hashtable_access(hashtable_t *tbl, void *key, void *value);
void hashtable_foreach(hashtable_t *tbl, visit_fn_t visit, void *state);

#endif // !defined (_CHASH_H)

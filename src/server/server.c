// -----------------------------------------------------] IMPORTS [---

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "../libs/chash.h"

// #include <errno.h>
// #include <sys/types.h>

// -----------------------------------------------------] CONFIGS [---

#define MEMBERS_ACTIVITY_CHECK 5

#define BUFFLEN 512
#define PORT 64546
#define HEADERTS 16
#define SOCKLEN sizeof(struct sockaddr_in)

int avstats[4] = { 0, 0, 0, 0 }; // Maske

#define WELCOME "[SERVER] Welcome, You have successfully connected to the server!\n"

// -----------------------------------------------------] OPCODES [---

// new USeR connected
#define OPCODE_USR 0x10
// ACKnowledgment
#define OPCODE_ACK 0x11
// EXiT
#define OPCODE_EXT 0x12
// CHecK client's state
#define OPCODE_CHK 0x13
// Avatar MoVed
#define OPCODE_AMV 0x14
// CriTical Error
#define OPCODE_CTE 0x15
// MeSsaGe
#define OPCODE_MSG 0x40

// -----------------------------------------------------] STRUCTS [---

typedef struct network {
  bool active;
  int *sockfd;
  char *out;
  hashtable_t *members;
} network_t;

// av_data is coded as 0 => id; 1 => x; 2 => y; 3 => direction; 4 => sprite;

typedef struct client {
  char nickname[20];
  int av_data[5], state_requested;
  long updated;
  struct sockaddr_in data;
} client_t;

// --------------------------------------------------] PROTOTYPES [---

char *s_addr_to_string(struct sockaddr_in *address);

int broadcast(int *sockfd, hashtable_t *members, char *packet);
int request_avatar_id(int *mask);
int check_name_exists(hashtable_t *members, char *nickname);

void *worker_chk_state(void * arguments);

// ---------------------------------------------------] FUNCTIONS [---

int broadcast(int *sockfd, hashtable_t *members, char *packet) {
  for (unsigned i = 0; i < members->capacity; i++)
  {
    if ((members->buckets[i].key) != NULL)
    {
      if (sendto(*sockfd, packet, strlen(packet), 0, (struct sockaddr *) &(*(client_t *) members->buckets[i].value).data, SOCKLEN) == -1)
      {
        return -1;
      }
    }
  }
  return 0;
}

int request_avatar_id(int *mask) {
  for (int i = 0; i < 4; i++)
  {
    if (!mask[i])
    {
      mask[i] = 1;
      return i;
    }
  }
  return rand() & 3;
}

char *s_addr_to_string(struct sockaddr_in *address) {
  char *s = (char *) calloc(24, sizeof(char));
  sprintf(s, "%s:%d", inet_ntoa((*address).sin_addr), (int) ntohs((*address).sin_port));
  return s;
}

int check_name_exists(hashtable_t *members, char *nickname) {
  for (unsigned i = 0; i < members->capacity; i++)
  {
    if ((members->buckets[i].key) != NULL)
    {
      if (!strcmp(nickname, (*(client_t *) members->buckets[i].value).nickname))
      {
        return 1;
      }
    }
  }
  return 0;
}

void *worker_chk_state(void *args) {
  network_t *chat = (network_t *) args;
  client_t *cl;
  char packet[2] = { OPCODE_CHK, 0 };
  while (chat->active)
  {
    for (unsigned i = 0; i < chat->members->capacity; i++)
    {
      if ((cl = (client_t *) chat->members->buckets[i].value) != NULL)
      {
        if (cl->state_requested)
        {
          sprintf(chat->out, "%c%s logged out", OPCODE_EXT, cl->nickname);
          avstats[cl->av_data[0]] = 0;
          printf("%s\n", &chat->out[1]);
          broadcast(chat->sockfd, chat->members, chat->out);
          hashtable_remove(chat->members, chat->members->buckets[i].key);
        } else
        {
          cl->state_requested = 1;
          sendto(*chat->sockfd, packet, strlen(packet), 0, (struct sockaddr *) &cl->data, SOCKLEN);
        }
      }
    }
    sleep(MEMBERS_ACTIVITY_CHECK);
  }
  return NULL;
}

int main(int argc, char **argv) {
  printf("-------------------------------------------------------\n");
  printf("# Projekt: Chat mit Hut v. 1.0.0 Final\n\n");
  printf("# B-BKSPP-PR\n");
  printf("# Authors: Nikolaj Schepsen & Rafael Franzke\n\n");

  printf("# Datum: 06-02-2014\n\n");

  printf("# SDL 1.2 http://www.libsdl.org/download-1.2.php\n");
  printf("# CMAKE 2.8 http://www.cmake.org/cmake/resources/software.html\n");
  printf("-------------------------------------------------------\n");

  socklen_t socklen = sizeof(struct sockaddr_in);

  char received[BUFFLEN], out[BUFFLEN], *key;
  int sockfd, n;
  srand(time(NULL));
  void **s;
  struct sockaddr_in server_addr, client_addr;

  pthread_t workers[1];

  if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
  {
    perror("An error occurred while creating socket");
    return EXIT_FAILURE;
  }

  memset((void *) &server_addr, 0, (size_t) socklen);

  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  server_addr.sin_port = htons(PORT);

  if (bind(sockfd, (struct sockaddr *) &server_addr, socklen))
  {
    perror("An error occurred while binding socket");
    return EXIT_FAILURE;
  }

  hashtable_t *members = hashtable_alloc(&stdhash, &stdcompare, 0);
  network_t network = { .active = true, .sockfd = &sockfd, .members = members, .out = out };

  if (pthread_create(&workers[0], NULL, &worker_chk_state, (void *) &network))
  {
    perror("An error occurred while creating a thread");
    return EXIT_FAILURE;
  }

  while (network.active)
  {
    if ((n = recvfrom(sockfd, received, BUFFLEN, 0, (struct sockaddr *) &client_addr, &socklen)) > 0)
    {
      received[n] = 0; /* null terminate */
      s = hashtable_access(members, key = s_addr_to_string(&client_addr), NULL);

      if (received[0] >= 0x40)
      {
        snprintf(out, HEADERTS + 2, "%c%s", OPCODE_ACK, &received[1]);
        if (sendto(sockfd, out, strlen(out), 0, (struct sockaddr *) &client_addr, socklen) == -1)
        {
          perror("An error occurred while sending acknowledgment");
        }
      }

      switch (received[0]) {
        case OPCODE_USR:
        {
          if (check_name_exists(members, &received[HEADERTS + 1]))
          {
            sprintf(out, "%cThe nickname \"%s\" has already been taken", OPCODE_CTE, &received[HEADERTS + 1]);

            if (sendto(sockfd, out, strlen(out), 0, (struct sockaddr *) &client_addr, socklen) == -1)
            {
              perror("An error occurred while sending name-in-use message");
            }
            printf("Someone tried to connect, but the chosen nickname \"%s\" is already in use\n", &received[HEADERTS + 1]);
          } else
          {
            client_t *cl = (client_t *) malloc(sizeof(client_t));

            cl->data = client_addr;
            cl->state_requested = 0;
            cl->updated = 0;
            strcpy(cl->nickname, &received[HEADERTS + 1]);
            cl->av_data[0] = request_avatar_id(avstats);

            // av_data is coded as 0 => id; 1 => x; 2 => y; 3 => direction; 4 => sprite;

            cl->av_data[1] = rand() & 1023;
            cl->av_data[2] = rand() & 511;
            cl->av_data[3] = rand() & 3;
            cl->av_data[4] = rand() & 3;

            sprintf(out, "%c%s", OPCODE_MSG, WELCOME);
            if (sendto(sockfd, out, strlen(out), 0, (struct sockaddr *) &cl->data, socklen) == -1)
            {
              perror("An error occurred while sending welcome message");
            }

            hashtable_access(members, (void *) key, (void *) cl);

            for (unsigned i = 0; i < members->capacity; i++)
            {
              client_t *mbr = (client_t *) members->buckets[i].value;
              if (mbr != NULL)
              {
                sprintf(out, "%c%d:%04d:%04d:%d:%d:%s", OPCODE_USR, mbr->av_data[0], mbr->av_data[1], mbr->av_data[2], mbr->av_data[3], mbr->av_data[4], mbr->nickname);
                sendto(sockfd, out, strlen(out), 0, (struct sockaddr *) &cl->data, socklen);
              }
            }
            sprintf(out, "%c%d:%04d:%04d:%d:%d:%s", OPCODE_USR, cl->av_data[0], cl->av_data[1], cl->av_data[2], cl->av_data[3], cl->av_data[4], cl->nickname);
            broadcast(&sockfd, members, out);
            printf("%s (%s:%d) connected\n", &out[17], inet_ntoa(cl->data.sin_addr), (int) ntohs(cl->data.sin_port));
          }
        }
        break;
        case OPCODE_AMV:
        {
          if (((**(client_t **) s).updated < atol(&received[15])))
          {
            (**(client_t **) s).updated = atol(&received[15]);
            sprintf(&received[15], "%s", (**(client_t **) s).nickname);
            broadcast(&sockfd, members, received);
            (**(client_t **) s).av_data[1] = atoi(strtok(&received[1], ":"));
            (**(client_t **) s).av_data[2] = atoi(strtok(&received[6], ":"));
            (**(client_t **) s).av_data[3] = atoi(strtok(&received[11], ":"));
            (**(client_t **) s).av_data[4] = atoi(strtok(&received[13], ":"));
          }
        }
        break;
        case OPCODE_EXT:
        {
          if (key != NULL)
          {
            avstats[(**(client_t **) s).av_data[0]] = 0;
            sprintf(out, "%c%s logged out", OPCODE_EXT, (**(client_t **) s).nickname);
            broadcast(&sockfd, members, out);

            printf("%s\n", &out[1]);
            hashtable_remove(members, (void *) key);
          }
        }
        break;
        case OPCODE_CHK:
        {
          (**(client_t **) s).state_requested = 0;
        }
        break;
        case OPCODE_MSG:
        {
          if (s != NULL)
          {
            sprintf(out, "%c%s: %s", OPCODE_MSG, (*(client_t *) (*s)).nickname, &received[HEADERTS + 1]);
            broadcast(&sockfd, members, out);
            printf(">> %s\n", &out[1]);
          }
        }
        break;
        default:
          perror("An unknown opcode was received");
        break;
      }
      if (received[0] != OPCODE_USR) free(key);
    }
  }

  pthread_cancel(workers[0]);
  pthread_join(workers[0], NULL);

  hashtable_free(members, &free, &free);
  close(sockfd);

  return EXIT_SUCCESS;
}

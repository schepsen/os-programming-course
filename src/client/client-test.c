// -----------------------------------------------------] IMPORTS [---

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <SDL/SDL.h>

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>

#include "../libs/chash.h"

// -----------------------------------------------------] CONFIGS [---

#define STORAGE_CLEANING_TIME 10

#define BUFFLEN 512
#define PORT 64546
#define HEADERTS 16

#define SPEED 10
#define MSGBOX_CAPACITY 47
#define FAILRATE 50

#define SOCKLEN sizeof(struct sockaddr_in)

// --------------------------------------------------] SDL CONFIG [---

#define FONT_SIZE 50

#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 768

#define FONT_WIDTH 7
#define FONT_HEIGHT 12
#define STRING_LENGTH 144
#define TEXTFIELD_POSY 747
#define RECT_HEIGHT 18
#define ULIST_POSX 60
#define ULIST_POSY 22

// -----------------------------------------------------] OPCODES [---

// new USeR connected
#define OPCODE_USR 0x10
// ACKnowledgment
#define OPCODE_ACK 0x11
// EXiT
#define OPCODE_EXT 0x12
// CHecK client's state
#define OPCODE_CHK 0x13
// Avatar MoVed
#define OPCODE_AMV 0x14
// CriTical Error
#define OPCODE_CTE 0x15
// MeSsaGe
#define OPCODE_MSG 0x40

// -----------------------------------------------------] STRUCTS [---

typedef struct avatar {
  char *desc, src[32];
  int x, y, direction, sprite;
  SDL_Surface *surface;
} avatar_t;

typedef struct network {
  int *sockfd;
  bool active;
  char *received, *out, *local;
  struct sockaddr_in *server;
  hashtable_t *avatars, *storage;
} network_t;

typedef struct UIState {
  int mouseX, mouseY, mousedown;
  int hotitem, activeitem, kbditem;
  int lastwidget;
  int keyentered, keymod, keychar;
  int msglen, msglines, firstfocus;
  char message[STRING_LENGTH];
} uistate_t;

// -----------------------------------------------------] GLOBALS [---

SDL_Surface *font, *screen;
uistate_t ui = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "Enter chat message here" };

char msgbox[MSGBOX_CAPACITY][STRING_LENGTH];

// 0 => id; 1 => r; 2 => g; 3 => b; 4 => Ges. Breite; 5 => Ges. Hoehe; 6 => Einzel. Breite; 7 => Einzel Hoehe;
int sprites[4][8] = { { 0, 255, 255, 255, 428, 384, 107, 96 }, { 1, 255, 255, 255, 256, 256, 64, 64 }, { 2, 0, 0, 0, 252, 255, 63, 64 }, { 3, 0, 255, 6, 192, 192, 48, 48 } };

// --------------------------------------------------] PROTOTYPES [---

void createpacket(int opcode, const char *string, char *out, hashtable_t *storage);
int sendpacket(int *sockfd, struct sockaddr_in *server, char *packet);
void *receivepackets(void *args);
void *find_unsent_messages(void * args);

void gui_board_insert(char *message);
void gui_board_update(char *message);

// ---------------------------------------------------] FUNCTIONS [---

void createpacket(int opcode, const char *string, char *out, hashtable_t *storage) {
  struct timeval ts;
  gettimeofday(&ts, NULL);

  long timestamp = (ts.tv_sec * 1000000) + ts.tv_usec;
  sprintf(out, "%c%ld%s", opcode, timestamp, string);

  if (opcode >= 0x40)
  {
    char *packet = calloc(strlen(out) + 1, sizeof(char));
    strncpy(packet, out, strlen(out));
    char *key = calloc(HEADERTS + 1, sizeof(char));
    snprintf(key, HEADERTS + 1, "%ld", timestamp);
    hashtable_access(storage, (void *) key, (void *) packet);
  }
}

int sendpacket(int *sockfd, struct sockaddr_in *server, char *packet) {
  int n = 0;
  if ((packet[0] != OPCODE_MSG) || ((rand() % 100) > FAILRATE))
  {
    n = sendto(*sockfd, packet, strlen(packet), 0, (struct sockaddr *) server, SOCKLEN);
  } else
  {
    puts("Oh je! Ich bin zu faul, deine Nachricht jetzt zu versenden!");
  }
  return n;
}

void *receivepackets(void *args) {
  network_t *net = (network_t *) args;
  int n;
  while (net->active)
  {
    n = recvfrom(*net->sockfd, net->received, BUFFLEN, 0, NULL, NULL);
    net->received[n] = 0x0;
    switch (net->received[0]) {
      case OPCODE_USR: // 0x10
      {
        if (hashtable_access(net->avatars, (void *) &net->received[17], NULL) == NULL)
        {
          avatar_t *av = (avatar_t *) calloc(1, sizeof(avatar_t));
          av->desc = calloc(20, sizeof(char));
          strcpy(av->desc, &net->received[17]);
          sprintf(av->src, "./data/sprites/avatar%c.bmp", net->received[1]);

          av->x = atoi(strtok(&net->received[3], ":"));
          av->y = atoi(strtok(&net->received[8], ":"));
          av->direction = atoi(strtok(&net->received[13], ":"));
          av->sprite = atoi(strtok(&net->received[15], ":"));

          SDL_Surface *image;

          if ((image = SDL_LoadBMP(av->src)) != NULL)
          {
            av->surface = SDL_DisplayFormat(image);
            SDL_FreeSurface(image);
          }
          sprintf(net->out, "[INFO] %s connected!", av->desc);
          gui_board_insert(net->out);

          hashtable_access(net->avatars, (void *) av->desc, (void *) av);
        }
      }
      break;
      case OPCODE_ACK: // 0x11
      {
        strncpy(net->out, &(net->received)[1], HEADERTS);
        net->out[HEADERTS] = 0;
        printf("Erhalte die Bestätigung, dass die Nachricht (id: %s) angekommen ist\n", net->out);
        hashtable_remove(net->storage, (void *) net->out);
      }
      break;
      case OPCODE_EXT: // 0x12
      {
        char *cl = strtok(&net->received[1], " ");
        sprintf(net->out, "[INFO] %s logged out\n", cl);
        avatar_t *shadow = *(avatar_t **) hashtable_access(net->avatars, (void *) cl, NULL);

        if (shadow != NULL)
        {
          SDL_FreeSurface(shadow->surface);
          hashtable_remove(net->avatars, (void *) cl);
        }
        gui_board_insert(net->out);
      }
      break;
      case OPCODE_CHK: // 0x13
      {
        sendpacket(net->sockfd, net->server, net->received);
      }
      break;
      case OPCODE_AMV: // 0x14
      {
        avatar_t *av = *(avatar_t **) hashtable_access(net->avatars, (void *) &net->received[15], NULL);
        av->x = atoi(strtok(&net->received[1], ":"));
        av->y = atoi(strtok(&net->received[6], ":"));
        av->direction = atoi(strtok(&net->received[11], ":"));
        av->sprite = atoi(strtok(&net->received[13], ":"));
      }
      break;
      case OPCODE_CTE: // 0x15
      {
        fprintf(stderr, "%s\n", &net->received[1]);
        net->active = 0;
      }
      break;
      case OPCODE_MSG: // 0x40
      {
        gui_board_insert(&net->received[1]);
      }
      break;
      default:
        perror("An unknown opcode was received");
      break;
    }
  }
  return NULL;
}

void *find_unsent_messages(void * args) {
  struct timeval ts;
  long current;
  network_t *net = (network_t *) args;

  while (net->active)
  {
    if (net->storage->size > 0)
    {
      gettimeofday(&ts, NULL);
      current = ((ts.tv_sec * 1000000) + ts.tv_usec) - (STORAGE_CLEANING_TIME * 1000000);
      unsigned i;
      for (i = 0; i < net->storage->capacity; i++)
      {
        if ((net->storage->buckets[i].key != NULL) && (atol(net->storage->buckets[i].key) < current))
        {
          printf("Versuche nochmal, deine Nachricht (id: %s) jetzt zu versenden...\n", (char *) net->storage->buckets[i].key);
          sendpacket(net->sockfd, net->server, (char *) net->storage->buckets[i].value);
        }
      }
    }
    sleep(STORAGE_CLEANING_TIME);
  }
  return NULL;
}

void drawstring(char *string, int x, int y) {
  SDL_Rect src, dst;
  char ch;
  while ((ch = *string++))
  {
    src.w = FONT_WIDTH;
    src.h = FONT_HEIGHT;
    src.x = 0;
    src.y = (ch - 32) * FONT_HEIGHT;
    dst.w = FONT_WIDTH;
    dst.h = FONT_HEIGHT;
    dst.x = x;
    dst.y = y;
    SDL_BlitSurface(font, &src, screen, &dst);
    x += FONT_WIDTH;
  }
}

void drawrect(int x, int y, int w, int h, int color) {
  SDL_Rect r;
  r.x = x;
  r.y = y;
  r.w = w;
  r.h = h;
  SDL_FillRect(screen, &r, color);
}

int regionhit(int x, int y, int w, int h) {
  if ((ui.mouseX < x) || (ui.mouseY < y) || (ui.mouseX >= (x + w)) || (ui.mouseY >= (y + h)))
  {
    return 0;
  }
  return 1;
}

void gui_board_update(char *message) {
  for (int i = 1; i < MSGBOX_CAPACITY; i++)
  {
    sprintf(msgbox[i - 1], "%s", msgbox[i]);
  }
  sprintf(msgbox[MSGBOX_CAPACITY - 1], "%s", message);
}

void gui_board_insert(char *message) {
  if (ui.msglines >= MSGBOX_CAPACITY)
  {
    gui_board_update(message);
  } else
  {
    sprintf(msgbox[ui.msglines++], "%s", message);
  }
}

void paintComponent(avatar_t *avatar) {
  SDL_Rect src, dst;
  int id = avatar->src[21] - '0', r = sprites[id][1], g = sprites[id][2], b = sprites[id][3];

  src.x = sprites[id][6] * avatar->sprite;
  src.y = sprites[id][7] * avatar->direction;
  src.w = sprites[id][6];
  src.h = sprites[id][7];
  dst.x = avatar->x;
  dst.y = avatar->y;
  dst.w = sprites[id][6] * avatar->sprite;
  dst.h = sprites[id][7] * avatar->direction;

  SDL_SetColorKey(avatar->surface, SDL_SRCCOLORKEY, SDL_MapRGB(avatar->surface->format, r, g, b));
  SDL_BlitSurface(avatar->surface, &src, screen, &dst);

  drawstring(avatar->desc, avatar->x, avatar->y - 15);
}

void freeavatars(void *ptr) {

  free((void *) ((avatar_t *) ptr)->desc); /* ist zugleich der Schluessel */
  SDL_FreeSurface(((avatar_t *) ptr)->surface);

  free(ptr);
}

void repaint(hashtable_t *avs) {
  drawrect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, 0x505050);
  // Top line
  drawrect(0, 0, WINDOW_WIDTH, RECT_HEIGHT, 0x3A3A3A);
  drawstring("Chat mit Hut (B-BKSPP-PR) || Nikolaj Schepsen & Rafael Franzke, 2014", 10, 3);
  // Border
  drawrect(200, RECT_HEIGHT, 2, WINDOW_HEIGHT - 2 * RECT_HEIGHT - 8, 0xffffff);
  // User List
  drawrect(0, RECT_HEIGHT, 200, RECT_HEIGHT, 0x242424);
  drawstring("[User List]", ULIST_POSX, ULIST_POSY);
  // Main Field
  drawrect(202, RECT_HEIGHT, WINDOW_WIDTH - 202, WINDOW_HEIGHT - 2 * RECT_HEIGHT - 8, 0x505050);

  for (int i = 0; i < MSGBOX_CAPACITY; i++)
  {
    drawstring(msgbox[i], 205, RECT_HEIGHT + 7 + i * 15);
  }
  ui.hotitem = 0;
  if (regionhit(4, TEXTFIELD_POSY - 4, STRING_LENGTH * FONT_WIDTH + 8, FONT_HEIGHT + 8))
  {
    ui.hotitem = 0x100;
    if (ui.activeitem == 0 && ui.mousedown) ui.activeitem = 0x100;
  }
  if (ui.kbditem == 0x100)
  {
    drawrect(2, TEXTFIELD_POSY - 6, STRING_LENGTH * FONT_WIDTH + 12, FONT_HEIGHT + 12, 0xe5e5e5);
    if (!ui.firstfocus)
    {
      ui.message[0] = 0;
      ui.firstfocus = 1;
    }
  }

  if (ui.activeitem == 0x100 || ui.hotitem == 0x100)
  {
    drawrect(4, TEXTFIELD_POSY - 4, STRING_LENGTH * FONT_WIDTH + 8, FONT_HEIGHT + 8, 0x5e5e5e);
  } else
  {
    drawrect(4, TEXTFIELD_POSY - 4, STRING_LENGTH * FONT_WIDTH + 8, FONT_HEIGHT + 8, 0x0);
  }
  drawstring(ui.message, 8, TEXTFIELD_POSY);
  if ((ui.msglen < STRING_LENGTH) && (ui.kbditem == 0x100) && ((SDL_GetTicks() >> 8) & 1)) drawstring("_", 8 + ui.msglen * FONT_WIDTH, TEXTFIELD_POSY);
  if (ui.mousedown == 0 && ui.hotitem == 0x100 && ui.activeitem == 0x100) ui.kbditem = 0x100;
  ui.lastwidget = 0x100;

  unsigned i, count = 0;
  for (i = 0; i < avs->capacity; i++)
  {
    if (avs->buckets[i].key != NULL)
    {
      paintComponent((avatar_t *) avs->buckets[i].value);
      drawstring((char *) ((avatar_t *) avs->buckets[i].value)->desc, 20, 50 + ((count++) * 15));
    }
  }

  if (ui.mousedown == 0) ui.activeitem = 0;
  else if (ui.activeitem == 0) ui.activeitem = -1;
  ui.keyentered = 0;
  ui.keychar = 0;

  SDL_UpdateRect(screen, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
  SDL_Delay(10);
}

void notify(network_t *network, avatar_t *av) {
  struct timeval ts;
  gettimeofday(&ts, NULL);
  long timestamp;
  if ((rand() % 100) < FAILRATE)
  {
    puts("Manipuliere Daten...");
    timestamp = (ts.tv_sec * 1000) + ts.tv_usec;
  } else
  {
    timestamp = (ts.tv_sec * 1000000) + ts.tv_usec;
  }
  sprintf(network->out, "%c%04d:%04d:%d:%d:%16ld", OPCODE_AMV, av->x, av->y, av->direction, av->sprite, timestamp);
  sendpacket(network->sockfd, network->server, network->out);
}

int main(int argc, char **argv) {

  if ((argc != 3) || (strlen(argv[2]) > 20))
  {
    printf("Usage: %s <address> <nickname>\n", argv[0]);
    return EXIT_FAILURE;
  }

  hashtable_t *avatars = hashtable_alloc(&stdhash, &stdcompare, 0);
  hashtable_t *storage = hashtable_alloc(&stdhash, &stdcompare, 0);

  int sockfd;
  char received[BUFFLEN], out[BUFFLEN];

  pthread_t workers[2];
  network_t network;

  struct sockaddr_in server_addr;

  if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
  {
    perror("An error occurred while creating socket");
    return EXIT_FAILURE;
  }

  memset(&server_addr, 0, sizeof(server_addr));

  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = inet_addr(argv[1]);
  server_addr.sin_port = htons(PORT);

  network.active = true;
  network.received = received;
  network.out = out;
  network.server = &server_addr;
  network.sockfd = &sockfd;
  network.avatars = avatars;
  network.storage = storage;

  if (SDL_Init(SDL_INIT_VIDEO) == -1)
  {
    fprintf(stderr, "An error occurred while initializing SDL: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }

  atexit(SDL_Quit);

  screen = SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, 32, SDL_SWSURFACE);
  SDL_EnableKeyRepeat(110, 110);
  SDL_WM_SetCaption("Chat mit Hut (B-BKSPP-PR), 2014", "Chat mit Hut (B-BKSPP-PR), 2014");
  SDL_EnableUNICODE(1);

  if (screen == NULL)
  {
    fprintf(stderr, "An error occurred while setting up SDL Video Mode: %s\n", SDL_GetError());
    exit(EXIT_FAILURE);
  }

  SDL_Surface *temp = SDL_LoadBMP("./data/fonts/font50.bmp");
  font = SDL_ConvertSurface(temp, screen->format, SDL_SWSURFACE);
  SDL_FreeSurface(temp);

  SDL_SetColorKey(font, SDL_SRCCOLORKEY, 0);

  createpacket(OPCODE_USR, argv[2], network.out, storage);
  sendpacket(network.sockfd, network.server, network.out);

  if (pthread_create(&workers[0], NULL, &receivepackets, (void *) &network))
  {
    perror("An error occurred while creating a thread");
    return EXIT_FAILURE;
  }

  if (pthread_create(&workers[1], NULL, &find_unsent_messages, (void *) &network))
  {
    perror("An error occurred while creating a thread");
    return EXIT_FAILURE;
  }

  while (network.active)
  {
    SDL_Event e;

    repaint(avatars);

    while (network.active && SDL_PollEvent(&e))
    {
      switch (e.type) {
        case SDL_MOUSEMOTION:
        {
          ui.mouseX = e.motion.x;
          ui.mouseY = e.motion.y;
        }
        break;
        case SDL_MOUSEBUTTONDOWN:
        {
          if (e.button.button == 1)
          {
            ui.mousedown = 1;
          }
        }
        break;
        case SDL_MOUSEBUTTONUP:
        {
          if (e.button.button == 1)
          {
            ui.mousedown = 0;
          }
        }
        break;
        case SDL_KEYDOWN:
          ui.keyentered = e.key.keysym.sym;
          ui.keymod = e.key.keysym.mod;

          avatar_t *ptr = *(avatar_t **) hashtable_access(avatars, (void *) argv[2], NULL);

          if (!(e.key.keysym.unicode & 0xFF80))
          {
            ui.keychar = e.key.keysym.unicode & 0x7f;
          }
          if (ui.keychar >= 32 && ui.keychar < 127 && ui.msglen < STRING_LENGTH)
          {
            ui.message[ui.msglen++] = ui.keychar;
            ui.message[ui.msglen] = 0;
          }

          switch (e.key.keysym.sym) {
            case SDLK_DOWN:
            {
              if ((*ptr).y <= (WINDOW_HEIGHT - SPEED))
              {
                ptr->y += SPEED;
                ptr->direction = 0; /* down */
                ptr->sprite = (ptr->sprite + 1) & 3;
                notify(&network, ptr);
              }
            }
            break;
            case SDLK_LEFT:
            {
              if ((*ptr).x >= SPEED)
              {
                ptr->x -= SPEED;
                ptr->direction = 1; /* left */
                ptr->sprite = (ptr->sprite + 1) & 3;
                notify(&network, ptr);
              }
            }
            break;
            case SDLK_RIGHT:
            {
              if ((*ptr).x <= (WINDOW_WIDTH - SPEED))
              {
                ptr->x += SPEED;
                ptr->direction = 2; /* right */
                ptr->sprite = (ptr->sprite + 1) & 3;
                notify(&network, ptr);
              }
            }
            break;
            case SDLK_UP:
            {
              if ((*ptr).y >= SPEED)
              {
                ptr->y -= SPEED;
                ptr->direction = 3; /* up */
                ptr->sprite = (ptr->sprite + 1) & 3;
                notify(&network, ptr);
              }
            }
            break;
            case SDLK_BACKSPACE:
              if (ui.msglen > 0)
              {
                ui.message[--ui.msglen] = 0;
              }
            break;
            case SDLK_RETURN:
              if (!strncmp(ui.message, "logout", 6))
              {
                sprintf(network.out, "%c", OPCODE_EXT);
                if (sendpacket(network.sockfd, network.server, network.out) == -1)
                {
                  perror("An error occurred while sending message");
                }
                network.active = 0;
              } else
              {
                createpacket(OPCODE_MSG, ui.message, network.out, storage);
                if (sendpacket(network.sockfd, network.server, network.out) == -1)
                {
                  perror("An error occurred while sending message");
                }
              }
              ui.message[0] = ui.msglen = 0x0;
            break;
            default:
            {
              /* perror("An unhandled key was pressed"); */
            }
            break;
          }
        break;
        case SDL_KEYUP:
          switch (e.key.keysym.sym) {
            case SDLK_ESCAPE:
            {
              network.active = false;
            }
            break;
            default:
            break;
          }
        break;
        case SDL_QUIT:
        {
          network.active = false;
        }
        break;
        default:
        {
          /* perror("An unhandled event was fired"); */
        }
        break;
      }
    }
  }

  for (int i = 0; i < 2; i++)
  {
    pthread_cancel(workers[i]);
    pthread_join(workers[i], NULL);
  }

  close(sockfd);

  SDL_FreeSurface(screen);
  SDL_FreeSurface(font);
  hashtable_free(storage, &free, &free);
  hashtable_free(avatars, NULL, &freeavatars);
  SDL_Quit();
  return EXIT_SUCCESS;
}
